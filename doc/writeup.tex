\documentclass{article}
\usepackage{babel}
\usepackage[a4paper, top = 20mm, bottom=20mm, right=20mm, left=20mm]{geometry}
\usepackage[
	% draft
	]{graphicx}
\usepackage{amsmath}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{circuitikz}
\usepackage[hidelinks]{hyperref}
\usepackage{listings}
\usepackage{amssymb}
\setlength{\parindent}{3mm}
\setlength{\parskip}{5mm}
\linespread{1.65}
\renewcommand{\figurename}{Fig.}

\title{
	\textsc{Microelectrónica}
	\thanks{Master en Ingeniería de Sistemas Electrónicos,
	Escuela Técnica Superior de Ingenieros de Telecomunicación,
	Madrid, España.} \\
	\textsc{Diseño de un Microchip Semiconductor CMOS en tecnología AMS
	0.35$\mu m$ C35B4C3 para la implementación de Redes Neuronales \\
	en Hardware Dedicado}\par
}
\date{\today}
\author{
	\href{mailto:ivan.martin.canton@upm.es}{Iván Martin}
	\hspace{2cm}
	\href{mailto:ignacio.amat@upm.es}{Ignacio Amat}
}

\lstset{
	captionpos=b,
	keepspaces=true,
	numbers=left,
	showspaces=false,
	showstringspaces=false,
	showtabs=false,
	frame=single
}

\begin{document}
\maketitle
\begin{figure}[h!]
	\centering
	\includegraphics[width = .7\linewidth, angle=-90]{./img/todo_layout_mc.png}
\end{figure}
\clearpage
\tableofcontents
\listoffigures
\clearpage
\listoftables
\clearpage
\section{Introducción}

El objetivo de esta práctica es implementar un circuito en hardware dedicado
para realizar la inferencia de redes neuronales. Para ello, utilizaremos
distintos módulos de electrónica digital, basándonos en la tecnología CMOS. Se
justificarán las decisiones de diseño adoptadas para la realización de todos
estos módulos y se caracterizarán adecuadamente.

En el diseño de los módulos se ha intentado minimizar el área consumida por
los circuitos. En las secciones donde el diseño no es enteramente propio (full
custom) se han usado células de librería CORELIBD proporcionada por el
fabricante, en este caso AMS \textit{Austria Mikro Systeme}. El proceso de
fabricación es en 0.35$\mu$m C35B4C3. En esta tecnología contamos con dos
capas de polisilicio, aunque solamente hemos necesitado una. Adicionalmente el
fabricante nos facilita hasta cuatro pistas de metal, de las cuales hemos
utilizado tres. La primera y segunda, MET1 y MET2, principalmente en el
enrutamiento dentro de cada célula. La tercera capa de metal, MET3 se ha usado
en la etapa final cuando hemos juntados los distintos \textit{bitslices}.

A continuación se muestra un diagrama de la ruta de datos del sistema que
se ha implementado:
\vspace{2\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = 0.4\linewidth]{./img/sistema.png}
	\caption{Esquema de bloques del sistema.}
\end{figure}

\clearpage

\section{Selector}

Tras observar el \textit{datapath} del enunciado, hemos deducido que este
módulo se podía implementar fácilmente con un multiplexor 2x1 por cada bit.
Para su diseño hemos escogido el módulo MUX2x1 de la librería CORELIBD.

\subsection{Diseño}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .7\linewidth]{./img/selector.png}
	\caption{Implementación mediante multiplexador de 2 a 1.}
\end{figure}

\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .7\linewidth]{./img/selector_layout.png}
	\caption{Diseño de la célula estándar.}
\end{figure}
\clearpage

\subsection{Caracterización}
\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = 0.9\linewidth]{./img/selector.pdf}
	\caption{Simulación de la respuesta ante estímulos.}
\end{figure}
\vspace{-.5\baselineskip}
Debido al retardo en la subida y bajado de las señales observamos
un glitch en forma de pico de anchura reducida.
\clearpage

\subsubsection{Área}

\vspace{\baselineskip}
\begin{lstlisting}[caption = Área total del Selector.]
*******************************************************************
Area and Density
*******************************************************************
Library     : G202_1
Cell        : selector
View        : maskLayout
Option      : current to bottom
Stop Level  : 31
Created     : UTC 2021.11.29 21:22:28.343

*******************************************************************

Region      : ((0.5 -17.35) (12.0 -17.35) (12.0 -4.25) (0.5 -4.25))
TotalArea=  150.650000
\end{lstlisting}
\vspace{2\baselineskip}
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Area ($\mu m$) \\
		\midrule
		$13.1\times{}10.4$ \\
		\bottomrule
	\end{tabular}
	\caption{Dimensiones del diseño Selector.}
\end{table}

\clearpage
\subsubsection{Retardo}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_BDE_SEL.pdf}
	\caption{Retardo BDE respecto de SEL de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 11.1319-10.75 = 0.3819n$ S \\
	$t_{p_{LH}} = 20.4578-20.25 = 0.2078n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_BDR_SEL.pdf}
	\caption{Retardo BDR respecto de SEL de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 20.457846-20.25 = 0.207846n$ S \\
	$t_{p_{HL}} = 26.040603-25.75 = 0.290603n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_S_SEL.pdf}
	\caption{Retardo S respecto de SEL de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 40.45784-40.25 = 0.20784n$ S \\
	$t_{p_{HL}} = 46.040603-45.75 = 0.290603n$ S
\end{center}
\clearpage
\subsubsection{Consumo de Potencia}

\vspace{\baselineskip}
Para el cálculo de la potencia estática hemos generado un esquemático
donde todas las entradas han sido estimuladas al mismo tiempo a $V_{DD}$,
en nuestro caso a 3.3V. En el gráfico siguiente apreciamos que la potencia
se mantiene fija como es de esperar. Se ha seguido este procedimiento para
el resto de caracterizaciones.
\vspace{2\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/selector_pstatic.pdf}
	\caption{Consumo de Potencia estática del Selector.}
\end{figure}
\vspace{2\baselineskip}
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value (fW) \\
		\midrule
		403.345 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia estática del Selector.}
\end{table}

\clearpage

Para caracterizar el consumo de potencia dinámico procedemos de manera
distinta. Primero hemos definido un esquemático de prueba en el que excitamos
cada una de las entradas con pulsos a distinta frecuencia. Lo hacemos de esta
manera para poder observar la respuesta de nuestro circuito con todas las
combinaciones posibles, altas o bajas entre las todas entradas. Observamos en
la salida \textsc{selector} una onda periódica correspondiente a las salidas
esperadas. Dado que trabajamos con tecnología CMOS, apreciamos picos elevados
de consumo cuando la señal \textsc{selector} conmuta del estado bajo al alto.
Cuando la señal conmuta en sentido contrario distinguimos un pico de menor
altura. De nuevo, el procedimiento para el resto de módulos será idéntico.
\vspace{2\baselineskip}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/selector_pdynamic.pdf}
	\caption{Consumo de Potencia dinámica del Selector.}
\end{figure}

\begin{center}
	$P_{average} = 23.71\mu$W\\
	$P_{average} >>  P_{static}\therefore P_{dynamic}\approx P_{average} =
	23.71\mu$W
\end{center}
La señal se repite cada $20n$S, por lo tanto tiene una frecuencia de $50M$Hz.

\vspace{\baselineskip}
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value (W) \\
		\midrule
		23.71 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia dinámica del Selector.}
\end{table}
\clearpage

\section{\textit{Buffer} Triestado}

Este módulo se utiliza para controlar el flujo de entrada de datos a nuestro
sistema. Cuando la señal \textsc{oe} se encentra a nivel bajo, la salida
(\textsc{BDS}) estará en alta impedancia, en caso contrario la salida será
igual a la entrada (\textsc{BDR}).

\vspace{-\baselineskip}
\subsection{Diseño}

Para la implementar la funcionalidad del \textit{buffer} triestado pedido
hemos decidido usar el \textit{BUFEX2} de la librería \textit{CORELIBD}.

\begin{figure}[h!]
	\centering
	\includegraphics[width = .7\linewidth]{./img/tristate_schem.png}
	\caption{Implementación mediante BUFEX2.}
\end{figure}

\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .9\linewidth]{./img/tristate.png}
	\caption{Diseño célula estándar.}
\end{figure}

\clearpage
\subsection{Caracterización}
\begin{figure}[h!]
	\centering
	\includegraphics[width = 0.9\linewidth]{./img/buffer.pdf}
	\caption{Simulación de la respuesta ante estímulos.}
\end{figure}
\clearpage
\subsubsection{Área}

\vspace{2\baselineskip}
\begin{lstlisting}[caption = Área total reportada por Cadence.]
*******************************************************************
Area and Density
*******************************************************************
Library     : G202_1
Cell        : tristate
View        : maskLayout
Option      : current to bottom
Stop Level  : 31
Created     : UTC 2021.11.30 01:18:38.632

*******************************************************************

Region      : ((0.5 -17.35) (16.2 -17.35) (16.2 -4.25) (0.5 -4.25))
TotalArea=  205.670000
\end{lstlisting}

\vspace{2\baselineskip}
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Area ($\mu m$) \\
		\midrule
		$13.1\times{}15.7$ \\
		\bottomrule
	\end{tabular}
	\caption{Dimensiones del diseño buffer triestado.}
\end{table}

\clearpage
\subsubsection{Retardo}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_BDR_BDS.pdf}
	\caption{Retardo BDR respecto de BDS de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 20.45588-20.25 = 0.20588n$ S \\
	$t_{p_{HL}} = 31.0396-30.75 = 0.2896n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_OE_BDS.pdf}
	\caption{Retardo OE respecto de BDS de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 20.4559-20.25 = 0.2059n$ S \\
	$t_{p_{HL}} = 11.0397-10.25 = 0.7897n$ S
\end{center}

\subsubsection{Consumo de Potencia}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/buffer_pstatic.pdf}
	\caption{Consumo de Potencia estática del Buffer.}
\end{figure}

\begin{center}
	$P_{static} = 579.345$fW
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/buffer_pdynamic.pdf}
	\caption{Consumo de Potencia dinámica del Buffer.}
\end{figure}

\begin{center}
	$P_{average} = 2.078\mu$W\\
	$P_{average} >>  P_{static}\therefore P_{dynamic}\approx P_{average} =
	2.078\mu$W
\end{center}

La señal se repite cada $20n$S, por lo tanto tiene una frecuencia de $50M$Hz.

\clearpage
\section{Full Adder Full Custom}
\subsection{Diseño}
\begin{figure}[h!]
	\centering
	\input{fa.tex}
	\caption{Circuito implementado.}
\end{figure}

Euler path elegido para la malla de pMos:
\vspace{-\baselineskip}
\begin{center}
MP2 - MP0 - MP1 - MP3 - MP4 - MP5 - MP6 - MP7 - MP8 - MP9 - MP10 - MP11
\end{center}

Euler path simétrico para la malla de nMos:
\vspace{-\baselineskip}
\begin{center}
MN4 - MN0 - MN1 - MN2 - MN3 - MN5 - MN6 - MN8 - MN7 - MN11 - MN9 - MN10
\end{center}
\clearpage
\phantom{}\vfill
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_schem.png}
	\caption{Esquemático implementado.}
\end{figure}
\vfill
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa.png}
	\caption{Diseño Full Adder Full Custom implementado.}
\end{figure}
\vfill
\clearpage
\phantom{}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_fc_sim.pdf}
	\caption{Simulación de estímulos.}
\end{figure}

\clearpage

\subsection{Caracterización}
\subsubsection{Area}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_area.png}
	\caption{Medida dimensiones.}
\end{figure}

Como se muestra en la figura las dimensiones de nuestra célula son de $13.1\mu
m$ de alto entre las pistas de metal \textit{vdd!} y \textit{gnd!} y $21.64\mu
m$ de ancho en las difusiones \textit{pplus} y \textit{nplus}. Las difusiones
de los transistores pMos se han puesto a $0.8\mu m$ y las de los nMos a $0.4\mu
m$.

\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Area ($\mu m$) \\
		\midrule
		$13.1\times{}21.64$ \\
		\bottomrule
	\end{tabular}
	\caption{Dimensiones del diseño Full Adder Full Custom.}
\end{table}
\clearpage
\subsubsection{Retardo}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_A_S.pdf}
	\caption{Retardo A respecto de S de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 41.156-40.75 = 0.406n$ S \\
	$t_{p_{LH}} = 80.3759-80.25 = 0.1259n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_A_Carry.pdf}
	\caption{Retardo A respecto de Carry de alto a bajo.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 80.43828-80.25 = 0.18828n$ S \\
	$t_{p_{HL}} = 120.75-120.4237 = 0.3263n$ S
\end{center}

\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_B_S.pdf}
	\caption{Retardo B respecto de S de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 41.156-40.25 = 0.906n$ S \\
	$t_{p_{LH}} = 60.75413-60.75 = 0.00413n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_B_Carry.pdf}
	\caption{Retardo B respecto de Carry de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 40.4237-40.25 = 0.1737n$ S \\
	$t_{p_{HL}} = 60.75-60.5305 = 0.2195n$ S
\end{center}

\clearpage

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_C_S.pdf}
	\caption{Retardo C respecto de S de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 41.156-40.25 = 0.906n$ S \\
	$t_{p_{LH}} = 51.334-50.75 = 0.584n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/tp_C_Carry.pdf}
	\caption{Retardo C respecto de Carry de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 40.424-40.25 = 0.174n$ S \\
	$t_{p_{HL}} = 51.131-50.75 = 0.381n$ S
\end{center}

\clearpage
\subsubsection{Consumo de Potencia}

\begin{figure}[h!]
	\centering
	\includegraphics[width = .8\linewidth]{./img/fa_fc_sp_sim.pdf}
	\caption{Potencia estática del Full Adder Full Custom.}
\end{figure}
\vfill
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value (pW) \\
		\midrule
		5.8247 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia estática del Full Adder Full Custom.}
\end{table}

\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[width = .8\linewidth]{./img/fa_fc_dyn_sim.pdf}
	\caption{Potencia dinámica del Full Adder Full Custom.}
\end{figure}
\vfill
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value (W) \\
		\midrule
		21.24E-6 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia dinámica del Full Adder Full Custom.}
\end{table}

\clearpage
\section{Full Adder Semi Custom}
\subsection{Diseño}
\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\begin{circuitikz}
	\draw
	(0,0) node (x1) [xor port, anchor=in 1] {}
	(x1.in 1) -- ++ (-0.25,0) node (A)[circ,left]{}-- ++ (-0.75,0) node[left]{A}
	(x1.in 2) -- ++ (-0.75,0) node (B)[circ,left]{}-- ++ (-0.25,0) node[left]{B}
	(x1.out)  node (o1)[circ]{}

	(6,-2) node (x2) [xor port, anchor=in 1] {}
	(x2.in 1) |- (o1)
	(x2.in 2) -- ++ (-3,0) node (m)[circ]{}-- ++ (-4,0) node (C) [left] {C\textsubscript{in}}

	(4,-4) node (n2) [nand port, anchor=in 1] {}
	(x1.out) |- (n2.in 2)
	(n2.in 1) -| (m)

	(4,-6) node (n3) [nand port, anchor=in 1] {}
	(A) |- (n3.in 1)
	(B) |- (n3.in 2)

	(6,-5) node (n4) [nand port, anchor=in 1] {}
	(n4.in 1) -| (n2.out)
	(n4.in 2) -| (n3.out)

	(n4.out) node [right] {C\textsubscript{out}}
	(x2.out) node [right] {S};
\end{circuitikz}
\caption{Circuito Full Adder Semi Custom implementado.}
\end{figure}
\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/FA_SC_schem.png}
	\caption{Esquemático implementado.}
\end{figure}
\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[height = .39\textheight, angle = 90]{./img/FA_SC.png}
	\caption{Diseño Full Adder Semi Custom implementado.}
\end{figure}
\clearpage
\phantom{}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_sc_sim.pdf}
	\caption{Simulación de estímulos.}
\end{figure}

\clearpage

\subsection{Caracterización}
\subsubsection{Área}

\vspace{2\baselineskip}
\begin{lstlisting}[caption = Área total reportada por Cadence.]
*****************************************************************
Area and Density
*****************************************************************
Library     : G202_1
Cell        : logicfulladder
View        : maskLayout
Option      : current to bottom
Stop Level  : 31
Created     : UTC 2021.11.30 12:08:11.090

*****************************************************************

Region      : ((-0.85 0.0) (30.25 0.0) (30.25 13.1) (-0.85 13.1))
TotalArea=  407.410000
\end{lstlisting}
\vspace{2\baselineskip}
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Area ($\mu m$) \\
		\midrule
		$13.1\times{}31.1$ \\
		\bottomrule
	\end{tabular}
	\caption{Dimensiones del diseño Full Adder Semi Custom.}
\end{table}
\clearpage
\subsubsection{Retardo}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_sc_A_S.pdf}
	\caption{Retardo A respecto de S de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 41.1568-40.7527 = 0.4041n$ S \\
	$t_{p_{LH}} = 80.3752-80.2473 = 0.1279n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_sc_A_Carry.pdf}
	\caption{Retardo A respecto de Carry de alto a bajo.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 80.4376-80.2473 = 0.1903n$ S \\
	$t_{p_{HL}} = 120.7527-120.4231 = 0.3296n$ S
\end{center}

\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_sc_B_S.pdf}
	\caption{Retardo B respecto de S de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 41.15684-40.2473 = 0.90954n$ S \\
	$t_{p_{LH}} = 60.7527-60.74584 = 0.00686n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_sc_B_Carry.pdf}
	\caption{Retardo B respecto de Carry de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 40.4208-40.2473 = 0.1735n$ S \\
	$t_{p_{HL}} = 60.7527-60.52973 = 0.22297n$ S
\end{center}

\clearpage

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_sc_C_S.pdf}
	\caption{Retardo C respecto de S de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 41.156-40.25 = 0.906n$ S \\
	$t_{p_{LH}} = 51.334-50.75 = 0.584n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/fa_sc_C_Carry.pdf}
	\caption{Retardo C respecto de Carry de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{LH}} = 40.4237-40.25 = 0.1737n$ S \\
	$t_{p_{HL}} = 51.131-50.75 = 0.381n$ S
\end{center}

\clearpage
\subsubsection{Consumo de Potencia}

\begin{figure}[h!]
	\centering
	\includegraphics[width = .8\linewidth]{./img/fa_sc_stat.pdf}
	\caption{Potencia estática del Full Adder Semi Custom.}
\end{figure}
\vfill
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value (pW) \\
		\midrule
		5.8247 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia estática del Full Adder Semi Custom.}
\end{table}

\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[width = .8\linewidth]{./img/fa_sc_dyn.pdf}
	\caption{Potencia dinámica del Full Adder Semi Custom.}
\end{figure}
\vfill
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value (W) \\
		\midrule
		21.24E-6 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia dinámica del Full Adder Semi Custom.}
\end{table}
\clearpage

\section{Bit Slice}

Después de analizar las prestaciones casi idénticas de los sumadores completos
desarrollados, procedemos a utilizar sumador \textit{Full Custom} en los
siguientes módulos. Está decisión ha sido realizada tras considerar que solo
ocupa 283.484$\mu m^{2}$m frente a los 407.41$\mu m^{2}$ del diseño
\textit{Semi Custom} realizado con puertas lógicas.

\subsection{Diseño}

Para el diseño del \textit{bitslice} hemos decidio excluir el módulo
correspondiente al comparador ya que no es idéntico en todos los casos. De
haber incluido este módulo el \textit{bitslice} no habría sido posible
teselarlos. En el \textit{bitslice} correspondiente al bit más significativo
en lugar de un comparador, pondremos un sumador completo.

\vspace{2\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/bitslice.png}
	\caption{Unión del Selector y el Full Adder Full Custom.}
\end{figure}

\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[width = 1.4\linewidth, angle=90]{./img/bitslice_layout.png}
	\caption{Diseño de la célula estándar.}
\end{figure}
\clearpage

\subsection{Caracterización}
\begin{figure}[h!]
	\centering
	\includegraphics[width = 0.9\linewidth]{./img/bitslice.pdf}
	\caption{Simulación de la respuesta ante estímulos.}
\end{figure}
\clearpage

\subsubsection{Área}
\vspace{2\baselineskip}
\begin{lstlisting}[caption = Área total reportada por Cadence.]
***********************************************************************
Area and Density
***********************************************************************
Library     : G202_1
Cell        : bitslice
View        : maskLayout
Option      : current to bottom
Stop Level  : 31
Created     : UTC 2021.11.30 21:41:36.349

***********************************************************************

Region      : ((0.5 -17.35) (34.365 -17.35) (34.365 -4.25) (0.5 -4.25))
TotalArea=  443.631500
\end{lstlisting}
\vspace{2\baselineskip}
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Area ($\mu m$) \\
		\midrule
		$13.1\times{}33.865$ \\
		\bottomrule
	\end{tabular}
	\caption{Dimensiones del diseño Selector.}
\end{table}
\clearpage
\subsubsection{Consumo de Potencia}

\begin{figure}[h!]
	\centering
	\includegraphics[width = .8\linewidth]{./img/bitslice_pstat.pdf}
	\caption{Consumo de Potencia estática del Bitslice.}
\end{figure}
\vfill
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value (fW) \\
		\midrule
		403.345 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia estática del Bitslice.}
\end{table}
\clearpage

\begin{figure}[h!]
	\centering
	\includegraphics[width = .8\linewidth]{./img/bitslice_pdyn.pdf}
	\caption{Consumo de Potencia dinámica del Bitslice.}
\end{figure}
\vfill
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value ($\mu$W) \\
		\midrule
		34.55 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia dinámica del Bitslice.}
\end{table}


\clearpage
\section{Comparador}

Para la realización de este módulo hemos optado por comprobar el signo del
resultado de una operación de resta. Para ello hemos implementado la sección
del acarreo de un sumador, ya que realmente la magnitud de la operación nos es
indiferente, tan solo nos interesa el signo. En el último bit se incorpora un
sumador completo, ya que la salida \textsc{sum} va a ser el signo de la resta.
En caso de que sea 0, entonces se cumple que A es mayor que B, en caso
contrario A será menor que B. En la entrada de B aplicamos además una
inversión, para poder llevar a cabo la resta con un bloque sumador.

\subsection{Diseño}
\vfill
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/comparador.png}
	\caption{Implementación del Comparador.}
\end{figure}
\vfill
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/comparador_layout.png}
	\caption{Diseño de la célula estándar.}
\end{figure}
\vfill
\clearpage

\subsection{Caracterización}
\begin{figure}[h!]
	\centering
	\includegraphics[width = 0.8\linewidth]{./img/comparador.pdf}
	\caption{Simulación de la respuesta ante estímulos.}
\end{figure}
\clearpage
\subsubsection{Área}
\vspace{2\baselineskip}
\begin{lstlisting}[caption = Área total reportada por Cadence.]
*****************************************************************
Area and Density
*****************************************************************
Library     : G202_1
Cell        : comparador
View        : maskLayout
Option      : current to bottom
Stop Level  : 31
Created     : UTC 2021.11.30 23:12:55.677

*****************************************************************

Region      : ((-0.85 0.0) (24.65 0.0) (24.65 13.1) (-0.85 13.1))
TotalArea=  334.050000
\end{lstlisting}
\vspace{2\baselineskip}
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Area ($\mu m$) \\
		\midrule
		$13.1\times{}25.5$ \\
		\bottomrule
	\end{tabular}
	\caption{Dimensiones del diseño Comparador.}
\end{table}

\clearpage
\subsubsection{Retardo}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/comparador_tp_A_Cout.pdf}
	\caption{Retardo Cout respecto de A de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 41.499-40.75 = 0.749n$ S \\
	$t_{p_{LH}} = 80.7927-80.25 = 0.5427n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/comparador_tp_B_Cout.pdf}
	\caption{Retardo Cout respecto de B de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 61.6767-60.75 = 0.9267n$ S \\
	$t_{p_{LH}} = 80.7927-80.25 = 0.5427n$ S
\end{center}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/comparador_tp_C_Cout.pdf}
	\caption{Retardo Cout respecto de C de alto a bajo y vice versa.}
\end{figure}

\begin{center}
	$t_{p_{HL}} = 71.109594-70.75 = 0.359594n$ S \\
	$t_{p_{LH}} = 80.792067-80.25 = 0.5427n$ S
\end{center}

\clearpage
\subsubsection{Consumo de Potencia}

\begin{figure}[h!]
	\centering
	\includegraphics[width = .8\linewidth]{./img/comparador_pstat.pdf}
	\caption{Consumo de Potencia estática del Comparador.}
\end{figure}
\vfill
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value (pW) \\
		\midrule
		4.254 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia estática del Comparador.}
\end{table}

\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[width = .81\linewidth]{./img/comparador_pdyn.pdf}
	\caption{Consumo de Potencia dinámica del Comparador.}
\end{figure}
\vfill
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value ($\mu$W) \\
		\midrule
		24.13 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia dinámica del Comparador.}
\end{table}

\clearpage
\section{Todo}

Finalmente, ponemos en común todos los bloques desarrollados a lo largo del
proyecto. Gracias a la metodología de diseño estructurando en
\textit{bitslice} este paso es sencillo. Como el sistema pedido es de 8 bits,
copiamos ocho veces los \textit{bitslices} con los compradores. Cabe destacar
que los 7 \textit{bitslice} de menor peso usan el comparador y el último el
sumador completo. El acarreo de entrada del comparador del bit menos
significativo está conectamos con $V_{dd}$, es decir añadimos `1'. De manera
análoga en el selector, cuando necesitamos de un `0' conectamos a tierra.

\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/todo.png}
	\caption{Implementación mediante módulos desarrollados anteriormente.}
\end{figure}

\clearpage
\phantom{}\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .8\linewidth]{./img/todo_symbol.png}
	\caption{Símbolo generado donde se aprecian entradas y salidas.}
\end{figure}

\clearpage
\phantom{}\vspace{2\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/todo_layout.png}
	\caption{Diseño implementado.}
\end{figure}

\clearpage
\phantom{}\vspace{2\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/todo_layout_detail.png}
	\caption{Diseño implementado detalle.}
\end{figure}
\clearpage

\phantom{}\vspace{2\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./img/todo_layout_detail1.png}
	\caption{Diseño implementado detalle.}
\end{figure}

\clearpage
\subsection{Diseño}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .9\linewidth]{./img/todo.pdf}
	\caption{Simulación de la respuesta ante estímulos.}
\end{figure}


\subsection{Caracterización}
\subsubsection{Área}
\vspace{2\baselineskip}
\begin{lstlisting}[caption = Área total reportada por Cadence.]
*******************************************************************************
Area and Density
*******************************************************************************
Library     : G202_1
Cell        : todo
View        : maskLayout
Option      : current to bottom
Stop Level  : 31
Created     : UTC 2021.12.01 12:59:15.756

*******************************************************************************

Region      : ((-1.825 -118.65) (70.88 -118.65) (70.88 -32.75) (-1.825 -32.75))
TotalArea=  6245.359500
\end{lstlisting}
\vspace{2\baselineskip}
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Area ($\mu m$) \\
		\midrule
		$85.9\times{}72.705$ \\
		\bottomrule
	\end{tabular}
	\caption{Dimensiones del diseño.}
\end{table}
\vspace{2\baselineskip}
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Area ($\mu m$) \\
		\midrule
		$72.8\times{}14.4$ \\
		\bottomrule
	\end{tabular}
	\caption{Región vacía del diseño.}
\end{table}

En términos porcentuales el espacio vació representa
en torno al 16.3\% del área total ocupada.

% total
% 72.705*85.9 = 6245.3595

% vacio
% x = 14.04*72.8 = 1022.112
% 100*(x/6253.52) = 16.344587

\clearpage
\subsubsection{Consumo de Potencia}
\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .9\linewidth]{./img/todo_pdyn.pdf}
	\caption{Consumo de Potencia dinámica de todo el sistema.}
\end{figure}

\vfill
\begin{table}[h!]
	\centering
	\begin{tabular}{cc}
		\toprule
		Value (W) \\
		\midrule
		98.37E-6 \\
		\bottomrule
	\end{tabular}
	\caption{Consumo de potencia dinámica del sistema.}
\end{table}
\vfill

\clearpage

\end{document}
