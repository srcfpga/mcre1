\babel@toc {nil}{}
\contentsline {section}{\numberline {1}Introducción}{8}{section.1}%
\contentsline {section}{\numberline {2}Selector}{9}{section.2}%
\contentsline {subsection}{\numberline {2.1}Diseño}{9}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Caracterización}{10}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Área}{11}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Retardo}{12}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {2.2.3}Consumo de Potencia}{14}{subsubsection.2.2.3}%
\contentsline {section}{\numberline {3}\textit {Buffer} Triestado}{16}{section.3}%
\contentsline {subsection}{\numberline {3.1}Diseño}{16}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Caracterización}{17}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Área}{18}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Retardo}{19}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Consumo de Potencia}{20}{subsubsection.3.2.3}%
\contentsline {section}{\numberline {4}Full Adder Full Custom}{21}{section.4}%
\contentsline {subsection}{\numberline {4.1}Diseño}{21}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Caracterización}{24}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Area}{24}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Retardo}{25}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Consumo de Potencia}{28}{subsubsection.4.2.3}%
\contentsline {section}{\numberline {5}Full Adder Semi Custom}{30}{section.5}%
\contentsline {subsection}{\numberline {5.1}Diseño}{30}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Caracterización}{33}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}Área}{33}{subsubsection.5.2.1}%
\contentsline {subsubsection}{\numberline {5.2.2}Retardo}{34}{subsubsection.5.2.2}%
\contentsline {subsubsection}{\numberline {5.2.3}Consumo de Potencia}{37}{subsubsection.5.2.3}%
\contentsline {section}{\numberline {6}Bit Slice}{39}{section.6}%
\contentsline {subsection}{\numberline {6.1}Diseño}{39}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Caracterización}{41}{subsection.6.2}%
\contentsline {subsubsection}{\numberline {6.2.1}Área}{42}{subsubsection.6.2.1}%
\contentsline {subsubsection}{\numberline {6.2.2}Consumo de Potencia}{43}{subsubsection.6.2.2}%
\contentsline {section}{\numberline {7}Comparador}{45}{section.7}%
\contentsline {subsection}{\numberline {7.1}Diseño}{45}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Caracterización}{46}{subsection.7.2}%
\contentsline {subsubsection}{\numberline {7.2.1}Área}{47}{subsubsection.7.2.1}%
\contentsline {subsubsection}{\numberline {7.2.2}Retardo}{48}{subsubsection.7.2.2}%
\contentsline {subsubsection}{\numberline {7.2.3}Consumo de Potencia}{50}{subsubsection.7.2.3}%
\contentsline {section}{\numberline {8}Todo}{52}{section.8}%
\contentsline {subsection}{\numberline {8.1}Diseño}{57}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}Caracterización}{58}{subsection.8.2}%
\contentsline {subsubsection}{\numberline {8.2.1}Área}{58}{subsubsection.8.2.1}%
\contentsline {subsubsection}{\numberline {8.2.2}Consumo de Potencia}{59}{subsubsection.8.2.2}%
